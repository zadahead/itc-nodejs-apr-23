
const { ObjectId } = require('mongodb');
const books = require('../utils/mongodb').books;
const query = require('../utils/postgres').query;


const services = {

    addBook: async ({ name, author, total_pages }) => {
        const resp = await query(`
            INSERT INTO books
            (name, author, total_pages)
            VALUES
            ($1, $2, $3)
            RETURNING ID;
        `, [name, author, total_pages])

        return resp.rows[0].id;
    },
    getAllBooks: async () => {
        const data = await query(`
            SELECT b.*, u.email, u.name as "user_name" 
            FROM books b
            INNER JOIN users u
            ON b.user_id = u.id
        `)
        return data.rows;
    },
    getBookById: async (bookId) => {
        const user = await query(`
            SELECT * FROM books
            WHERE id = $1
        `, [bookId])

        return user.rows[0];
    },
    updateBook: async (bookId, { name, author, total_pages }) => {
        await query(`
            UPDATE books
            SET 
                name = $1,
                author = $2,
                total_pages = $3
            WHERE id = $4
        `, [name, author, total_pages, bookId]);
    },
    deleteBook: async (bookId) => {
        await books().deleteOne({ _id: new ObjectId(bookId) });
    }
}

module.exports = services;