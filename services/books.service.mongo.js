
const { ObjectId } = require('mongodb');
const books = require('../utils/mongodb').books;


const services = {

    addBook: async (body) => {
        const resp = await books().insertOne(body); //3
        return resp.insertedId.toString();
    },
    getAllBooks: async () => {
        const data = await books().find({}).toArray();
        return data;
    },
    getBookById: async (bookId) => {
        const user = await books().findOne({ _id: new ObjectId(bookId) }); //1
        return user;
    },
    updateBook: async (bookId, body) => {
        await books().updateOne({ _id: new ObjectId(bookId) }, { $set: body });
    },
    deleteBook: async (bookId) => {
        await books().deleteOne({ _id: new ObjectId(bookId) });
    }
}

module.exports = services;