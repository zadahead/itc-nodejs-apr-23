
const { ObjectId } = require('mongodb');
const users = require('../utils/mongodb').users;
const query = require('../utils/postgres').query;

const services = {
    normalize: (body) => {
        if (body.age) {
            body.age = +body.age;
        }
    },

    clearUser: (user) => {
        delete user.password;
    },

    addUser: async ({ name, age, email, password }) => {
        try {
            const resp = await query(`
                INSERT INTO users
                (name, age, email, password)
                VALUES
                ($1, $2, $3, $4)
                RETURNING id;
            `,
                [
                    name, age, email, password
                ]);


            return resp.rows[0].id;
        } catch (error) {
            console.log(error);
            return {}
        }
    },
    addNormalizedUser: (body, hash) => {
        services.normalize(body);
        return services.addUser({ ...body, password: hash });
    },
    getAllUsers: async () => {
        const data = await query(`
            SELECT * 
            FROM users 
            ORDER BY id
        `);
        return data.rows;
    },
    getUserById: async (userId) => {
        const user = await query(`
            SELECT * FROM users
            WHERE id = $1
        `, [userId])
        return user.rows[0];
    },
    getUserByEmail: async (email) => {
        const user = await query(`
            SELECT * FROM users
            WHERE email = $1
        `, [email])
        return user.rows[0];
    },
    getUsersByQuery: async (q = "", sort = "id", direction = "asc", limit = "10", skip = "0") => {


        const data = await query(`
            SELECT * FROM users
            WHERE name ILIKE '%${q}%' or email ILIKE '%${q}%'
            ORDER BY ${sort} ${direction}
            LIMIT $1
            OFFSET $2
        `, [limit, skip])

        return data.rows;
    },
    getUsersByQueryMongo: async (q = "", sort = "_id", direction = "1", limit = "10", skip = "0") => {
        const query = new RegExp(q, 'i');

        const data = await users().find({
            $or: [
                { "email": query },
                { "name": query }
            ]
        }).sort({ [sort]: +direction }).limit(+limit).skip(+skip).toArray() //2

        return data;
    },
    updateUser: async (userId, body) => {
        await query(`
            UPDATE users 
            SET 
                ${Object.keys(body).map(key => `${key} = '${body[key]}'`).join(', ')}
            WHERE id = $1
        `, [
            userId
        ]);
    },
    updateNormalizedUser: async (userId, body) => {
        services.normalize(body);
        await services.updateUser(userId, body);
    },
    deleteUser: async (userId) => {
        await query(`
            DELETE FROM users 
            WHERE id = $1
        `, [userId]);
    }
}

module.exports = services;