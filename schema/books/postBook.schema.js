
const schema = {
    type: "object",
    properties: {
        name: {
            type: "string",
            isNotEmpty: true
        },
        author: {
            type: "string",
            isNotEmpty: true
        },
        total_pages: {
            type: "string",
            isNumber: true
        },
    },
    required: ["name", "author", "total_pages"],
    additionalProperties: false,
}

module.exports = schema;