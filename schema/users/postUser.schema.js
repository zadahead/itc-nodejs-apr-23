const schema = {
    type: "object",
    properties: {
        name: {
            type: "string",
            isNotEmpty: true
        },
        age: {
            type: "string",
            isNumber: true
        },
        email: {
            type: 'string',
            format: 'email'
        },
        password: {
            type: "string",
            isNotEmpty: true
        },
    },
    required: ["name", "age", "email", "password"],
    additionalProperties: false,
}

module.exports = schema;