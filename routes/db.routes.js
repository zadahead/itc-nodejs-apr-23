const express = require('express');
const fs = require('fs');

const router = express.Router();

router.post('/:name', (req, res) => {
    const { name } = req.params;
    const obj = Object.keys(req.body).length ? req.body : [];

    fs.writeFileSync(`db/${name}.json`, JSON.stringify(obj, null, 2));

    res.send('ok');

})

router.get('/:name', (req, res) => {
    const { name } = req.params;

    const content = fs.readFileSync(`db/${name}.json`, 'utf-8');

    res.send(JSON.parse(content));
})

module.exports = router;



/*
    id = 1;
    id = 2;
    id = 3;

    id = asdsaqd1D@!##d3d3d2;
    id = asd3qd$F%G#$G#G$#43g;
    id = HG^H%^%Fdgdfgdfgdffgd;



    key = 123
    password => 1234 => ASD#@#aesd2432f => 1234
*/