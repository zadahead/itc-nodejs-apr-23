const express = require('express');
const validate = require('../utils/schemaValidate');
const postUserSchema = require('../schema/users/postUser.schema');
const usersController = require('../controllers/users.controller');

const router = express.Router();

router.post('/', validate(postUserSchema), usersController.addUser);
router.get('/', usersController.getUsers);
router.get('/me', usersController.getMe);
router.get('/search', usersController.search);
router.get('/:userId', usersController.getUserById);
router.put('/:userId', usersController.updateUser);
router.delete('/:userId', usersController.deleteUser);


module.exports = router;