const express = require('express');
const validate = require('../utils/schemaValidate');
const postBookSchema = require('../schema/books/postBook.schema');
const booksController = require('../controllers/books.controller');

const router = express.Router();

router.post('/', validate(postBookSchema), booksController.addBook);
router.get('/', booksController.getBooks);

router.get('/:bookId', booksController.getBookById);
router.put('/:bookId', booksController.updateBook);
router.delete('/:bookId', booksController.deleteBook);


module.exports = router;