const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = `mongodb+srv://${process.env.MONGO_SECRET}@itcapr23.yuterdm.mongodb.net/?retryWrites=true&w=majority`;

const client = new MongoClient(uri, {
    serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: true
    }
})

let db;

async function run() {
    try {
        await client.connect();

        db = client.db('ITCMain', { ping: 1 });

        // const list = await users.find({}).toArray();

        console.log("Mongo is connected")


        //connect to a collection 'users'
        //show all list of all 'users' documents (find => toArray)

    } catch (error) {
        console.error(error)
    } finally {
        //await client.close();
    }
}

module.exports = {
    run,
    users: () => {
        return db.collection('users');
    },
    books: () => {
        return db.collection('books');
    }
}