const jwt = require('jsonwebtoken');
const sercetKey = process.env.JWT_SECRET_KEY; //value you can change based on evnviroment

module.exports = {
    sign: (payload) => {
        const accessToken = jwt.sign(payload, sercetKey, { expiresIn: '3d' });
        return {
            accessToken
        }
    },
    verify: (token) => {
        const payload = jwt.verify(token, sercetKey);
        return payload;
    }
}