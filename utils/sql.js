const { Client } = require('pg');

const client = new Client({
    host: 'dbtestz2.cvvrivmzth0x.eu-central-1.rds.amazonaws.com',
    port: 5432,
    database: 'mosh',
    user: 'postgres',
    password: 'hellomosh',
    ssl: {
        rejectUnauthorized: false
    }
})


let db;

async function run() {
    try {
        await client.connect();

        db = client;

        console.log("PostgreSQL is connected");

    } catch (error) {
        console.error(error)
    }
}

module.exports = {
    run,
    query: async (q, args) => {
        try {
            return await db.query(q, args);
        } catch (error) {
            console.error(err);
        } finally {
            await client.end()
        }
    }
}