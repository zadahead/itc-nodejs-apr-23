require('dotenv').config();
require('./utils/mongodb').run();
require('./utils/postgres').run();

const express = require('express');
const cors = require('cors');
const { ERR_TOKEN_NOT_VERIFIED } = require('./utils/errors');
const jwt = require('./utils/jwt');
const usersService = require('./services/users.service');

const app = express();


//middleware
app.use(cors({
    origin: '*'
}));
app.use(express.json());

app.use(async (req, res, next) => {
    console.log('method:', req.method);
    console.log('url:', req.url);

    const publicRoutes = [
        { method: 'POST', url: '/auth/login' },
        { method: 'POST', url: '/auth/register' }
    ]

    const isPublic = publicRoutes.find(endpoint => (
        endpoint.method === req.method && endpoint.url === req.url
    ));


    if (true) {
        return next();
    }

    //
    const { authorization: token } = req.headers;

    if (!token) {
        return next(ERR_TOKEN_NOT_VERIFIED)
    }

    try {
        const payload = jwt.verify(token);

        console.log(payload);

        const user = await usersService.getUserById(payload.userId);
        if (!user) {
            return next(ERR_TOKEN_NOT_VERIFIED)
        }

        //set permission 
        if (!user.permissions) {
            user.permissions = [];
        }
        const permission = {}
        user.permissions.forEach(p => permission[p] = true)

        user.permission = permission;

        req.user = user;

        /*
            verify token with the authorization header
            get the userId from the payload 
            get the user with the userId
            set req.user = user;
            go next
        */

        return next();
    } catch (error) {
        return next(ERR_TOKEN_NOT_VERIFIED);
    }
})


//routes

app.use('/users', require('./routes/users.routes'));
app.use('/books', require('./routes/books.routes'));
app.use('/db', require('./routes/db.routes'));
app.use('/auth', require('./routes/auth.routes'));


//error handling 
app.use((err, req, res, next) => {
    console.log(err);

    try {
        const [statusCode, msg] = err;

        res.status(statusCode).send({
            error: true,
            message: msg
        })
    } catch (error) {
        res.status(500).send({
            error: true,
            message: err.message
        })
    }
})

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log(`express is listening on port: ${PORT}`);
})
