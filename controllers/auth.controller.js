const usersService = require('../services/users.service');
const { validatePassword, hashPassword } = require('../utils/bycrypt');
const { ERR, LOGIN_NOT_AUTH, REGISTER_ALREADY_EXIST } = require('../utils/errors');
const jwt = require('../utils/jwt');


module.exports = {
    loginUser: async (req, res, next) => {
        try {
            const { email, password } = req.body;

            const user = await usersService.getUserByEmail(email);
            console.log(user);

            if (!user) {
                return next(LOGIN_NOT_AUTH);
            }

            const isValid = await validatePassword(password, user.password);

            if (!isValid) {
                return next(LOGIN_NOT_AUTH);
            }

            usersService.clearUser(user);

            const payload = {
                userId: user._id
            }

            //jwt
            const accessToken = jwt.sign(payload);


            res.send(accessToken);
        } catch (error) {
            console.log(error);
            next(ERR);
        }
    },

    registerUser: async (req, res, next) => {
        try {
            const { password, email } = req.body;

            const user = await usersService.getUserByEmail(email);

            if (user) {
                return next(REGISTER_ALREADY_EXIST);
            }

            const hash = await hashPassword(password);

            const newId = await usersService.addNormalizedUser(req.body, hash);

            res.send({ id: newId });

        } catch (error) {
            next(ERR);
        }
    }
}