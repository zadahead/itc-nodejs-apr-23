const booksService = require('../services/books.service');

const { ERR } = require('../utils/errors');


module.exports = {

    addBook: async (req, res, next) => {
        try {
            const newId = await booksService.addBook(req.body);

            res.send({ id: newId });

        } catch (error) {
            next(ERR);
        }
    },
    getBooks: async (req, res, next) => {
        try {
            const data = await booksService.getAllBooks();
            res.send(data);
        } catch (error) {
            next(ERR);
        }
    },
    getBookById: async (req, res, next) => {
        try {
            const { bookId } = req.params;
            const book = await booksService.getBookById(bookId);
            res.send(book);
        } catch (error) {
            next(ERR);
        }

    },
    updateBook: async (req, res, next) => {
        try {
            const { bookId } = req.params;
            await booksService.updateBook(bookId, req.body);
            res.send('updated');
        } catch (error) {
            console.log(error);
            next(ERR);
        }
    },
    deleteBook: async (req, res, next) => {
        try {
            const { bookId } = req.params;
            await booksService.deleteBook(bookId);
            res.send('deleted');
        } catch (error) {
            next(ERR);
        }

    }
}